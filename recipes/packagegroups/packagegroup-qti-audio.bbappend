#SUMMARY = "QTI Audio Package Group"

LICENSE = "BSD-3-Clause"

RDEPENDS_packagegroup-qti-audio += ' \
    agmplugin \
    agm-plugin-test \
    agm \
    agm-client \
    agm-server \
'
